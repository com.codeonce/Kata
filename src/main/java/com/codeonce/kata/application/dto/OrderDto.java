package com.codeonce.kata.application.dto;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Data Transfer Object representing an order")
public record OrderDto(
        @Schema(description = "Unique identifier of the order", example = "1234")
        String id,

        @Schema(description = "Unique identifier of the product associated with the order", example = "5678")
        String productId,

        @Schema(description = "Quantity of the product in the order", example = "10")
        int quantity
) {}
