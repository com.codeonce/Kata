package com.codeonce.kata.application.dto;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Data Transfer Object representing a product")
public record ProductDto(
        @Schema(description = "Unique identifier of the product", example = "1")
        Long id,

        @Schema(description = "Name of the product", example = "Laptop")
        String name,

        @Schema(description = "Description of the product", example = "A high-performance laptop for gaming and work")
        String description,

        @Schema(description = "Price of the product", example = "1299.99")
        double price,

        @Schema(description = "Unique identifier of the store associated with the product", example = "store123")
        String storeId
) {}
