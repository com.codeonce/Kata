package com.codeonce.kata.application.dto;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Data Transfer Object representing a store")
public record StoreDto(
        @Schema(description = "Unique identifier of the store", example = "1")
        Long id,

        @Schema(description = "Name of the store", example = "TechStore")
        String name,

        @Schema(description = "Address of the store", example = "123 Tech Avenue, Silicon Valley, CA")
        String address
) {}
