package com.codeonce.kata.application.mapper;

import com.codeonce.kata.application.dto.OrderDto;
import com.codeonce.kata.domain.model.Order;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OrderMapper {
    Order toDomain(OrderDto orderDto);

    OrderDto toDto(Order order);
}
