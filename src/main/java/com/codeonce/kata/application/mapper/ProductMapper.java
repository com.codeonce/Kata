package com.codeonce.kata.application.mapper;

import com.codeonce.kata.application.dto.ProductDto;
import com.codeonce.kata.domain.model.Product;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductMapper {
    Product toDomain(ProductDto productDto);

    ProductDto toDto(Product product);
}
