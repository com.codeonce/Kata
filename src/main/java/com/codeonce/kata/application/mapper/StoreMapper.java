package com.codeonce.kata.application.mapper;

import com.codeonce.kata.application.dto.StoreDto;
import com.codeonce.kata.domain.model.Store;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface StoreMapper {
    Store toDomain(StoreDto storeDto);

    StoreDto toDto(Store store);
}
