package com.codeonce.kata.application.rest.ordercontroller;

import com.codeonce.kata.application.dto.OrderDto;
import com.codeonce.kata.application.mapper.OrderMapper;
import com.codeonce.kata.domain.port.api.OrderServicePort;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/orders")
@Tag(name = "Order Controller", description = "API for managing orders")
public class OrderController {

    private final OrderServicePort orderServicePort;
    private final OrderMapper orderMapper;

    public OrderController(OrderServicePort orderServicePort, OrderMapper orderMapper) {
        this.orderServicePort = orderServicePort;
        this.orderMapper = orderMapper;
    }

    @Operation(summary = "Create a new order", description = "Creates a new order and returns the created order's details.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Order created successfully"),
            @ApiResponse(responseCode = "400", description = "Invalid input")
    })
    @PostMapping
    public ResponseEntity<OrderDto> createOrder(@RequestBody OrderDto orderDto) {
        OrderDto createdOrder = orderMapper.toDto(orderServicePort.createOrder(orderMapper.toDomain(orderDto)));
        return new ResponseEntity<>(createdOrder, HttpStatus.CREATED);
    }

    @Operation(summary = "Get all orders", description = "Retrieves a list of all orders.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List of orders retrieved successfully")
    })
    @GetMapping
    public ResponseEntity<List<OrderDto>> getAllOrders() {
        List<OrderDto> orders = orderServicePort.getAllOrders().stream().map(orderMapper::toDto).toList();
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }
}
