package com.codeonce.kata.application.rest.productcontroller;

import com.codeonce.kata.application.dto.ProductDto;
import com.codeonce.kata.application.mapper.ProductMapper;
import com.codeonce.kata.domain.port.api.ProductServicePort;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
@Tag(name = "Product Controller", description = "API for managing products")
public class ProductController {

    private final ProductServicePort productServicePort;
    private final ProductMapper productMapper;

    public ProductController(ProductServicePort productServicePort, ProductMapper productMapper) {
        this.productServicePort = productServicePort;
        this.productMapper = productMapper;
    }

    @Operation(summary = "Create a new product", description = "Creates a new product and returns the created product's details.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Product created successfully"),
            @ApiResponse(responseCode = "400", description = "Invalid input")
    })
    @PostMapping
    public ResponseEntity<ProductDto> createProduct(@RequestBody ProductDto productDto) {
        ProductDto createdProduct = productMapper.toDto(productServicePort.createProduct(productMapper.toDomain(productDto)));
        return new ResponseEntity<>(createdProduct, HttpStatus.CREATED);
    }

    @Operation(summary = "Get all products", description = "Retrieves a list of all products.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List of products retrieved successfully")
    })
    @GetMapping
    public ResponseEntity<List<ProductDto>> getAllProducts() {
        List<ProductDto> products = productServicePort.getAllProducts().stream().map(productMapper::toDto).toList();
        return new ResponseEntity<>(products, HttpStatus.OK);
    }
}
