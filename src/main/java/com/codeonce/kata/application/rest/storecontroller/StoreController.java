package com.codeonce.kata.application.rest.storecontroller;

import com.codeonce.kata.application.dto.StoreDto;
import com.codeonce.kata.application.mapper.StoreMapper;
import com.codeonce.kata.domain.port.api.StoreServicePort;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/stores")
@Tag(name = "Store Controller", description = "API for managing stores")
public class StoreController {

    private final StoreServicePort storeServicePort;
    private final StoreMapper storeMapper;

    public StoreController(StoreServicePort storeServicePort, StoreMapper storeMapper) {
        this.storeServicePort = storeServicePort;
        this.storeMapper = storeMapper;
    }

    @Operation(summary = "Get all stores", description = "Retrieves a list of all stores.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List of stores retrieved successfully")
    })
    @GetMapping
    public ResponseEntity<List<StoreDto>> getAllStores() {
        List<StoreDto> stores = storeServicePort.getAllStores().stream().map(storeMapper::toDto).toList();
        return new ResponseEntity<>(stores, HttpStatus.OK);
    }
}
