package com.codeonce.kata.domain.port.api;

import com.codeonce.kata.domain.model.Order;

import java.util.List;

public interface OrderServicePort {
    Order createOrder(Order order);
    List<Order> getAllOrders();
}
