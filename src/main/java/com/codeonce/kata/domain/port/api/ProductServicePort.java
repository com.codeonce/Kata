package com.codeonce.kata.domain.port.api;

import com.codeonce.kata.domain.model.Product;

import java.util.List;

public interface ProductServicePort {
    Product createProduct(Product product);
    List<Product> getAllProducts();
}
