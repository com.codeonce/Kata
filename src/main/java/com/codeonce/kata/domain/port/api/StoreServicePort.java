package com.codeonce.kata.domain.port.api;

import com.codeonce.kata.domain.model.Store;

import java.util.List;

public interface StoreServicePort {
    List<Store> getAllStores();
}
