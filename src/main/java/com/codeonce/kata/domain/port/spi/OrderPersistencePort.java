package com.codeonce.kata.domain.port.spi;

import com.codeonce.kata.domain.model.Order;

import java.util.List;

public interface OrderPersistencePort {
    Order createOrder(Order order);

    List<Order> getAllOrders();
}
