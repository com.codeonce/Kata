package com.codeonce.kata.domain.port.spi;

import com.codeonce.kata.domain.model.Order;

public interface OrderPublishPort {
    void publish(Order order);
}
