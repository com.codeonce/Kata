package com.codeonce.kata.domain.port.spi;

import com.codeonce.kata.domain.model.Product;

import java.util.List;

public interface ProductPersistencePort {
    Product createProduct(Product product);

    List<Product> getAllProducts();
}
