package com.codeonce.kata.domain.port.spi;

import com.codeonce.kata.domain.model.Store;

import java.util.List;

public interface StorePersistencePort {
    List<Store> getAllStores();
}
