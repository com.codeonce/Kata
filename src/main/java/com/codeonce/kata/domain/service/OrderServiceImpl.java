package com.codeonce.kata.domain.service;

import com.codeonce.kata.domain.model.Order;
import com.codeonce.kata.domain.port.api.OrderServicePort;
import com.codeonce.kata.domain.port.spi.OrderPersistencePort;
import com.codeonce.kata.domain.port.spi.OrderPublishPort;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderServicePort {
    private final OrderPersistencePort orderPersistencePort;
    private final OrderPublishPort orderPublishPort;
    private final Counter orderCounter;

    public OrderServiceImpl(OrderPersistencePort orderPersistencePort, OrderPublishPort orderPublishPort, MeterRegistry meterRegistry) {
        this.orderPersistencePort = orderPersistencePort;
        this.orderPublishPort = orderPublishPort;
        this.orderCounter = meterRegistry.counter("orders.created.total");
    }

    @Override
    public Order createOrder(Order order) {
        orderPublishPort.publish(order);
        orderCounter.increment();
        return orderPersistencePort.createOrder(order);
    }

    @Override
    public List<Order> getAllOrders() {

        return orderPersistencePort.getAllOrders();
    }
}
