package com.codeonce.kata.domain.service;

import com.codeonce.kata.domain.model.Product;
import com.codeonce.kata.domain.port.api.ProductServicePort;
import com.codeonce.kata.domain.port.spi.ProductPersistencePort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductServicePort {

    private final ProductPersistencePort productPersistencePort;

    public ProductServiceImpl(ProductPersistencePort productPersistencePort) {
        this.productPersistencePort = productPersistencePort;
    }

    @Override
    public Product createProduct(Product product) {
        return productPersistencePort.createProduct(product);
    }

    @Override
    public List<Product> getAllProducts() {
        return productPersistencePort.getAllProducts();
    }
}
