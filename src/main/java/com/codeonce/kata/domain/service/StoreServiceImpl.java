package com.codeonce.kata.domain.service;

import com.codeonce.kata.domain.model.Store;
import com.codeonce.kata.domain.port.api.StoreServicePort;
import com.codeonce.kata.domain.port.spi.StorePersistencePort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StoreServiceImpl implements StoreServicePort {

    private final StorePersistencePort storePersistencePort;

    public StoreServiceImpl(StorePersistencePort storePersistencePort) {
        this.storePersistencePort = storePersistencePort;
    }

    @Override
    public List<Store> getAllStores() {
        return storePersistencePort.getAllStores();
    }
}
