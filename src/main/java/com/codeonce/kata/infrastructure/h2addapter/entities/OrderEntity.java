package com.codeonce.kata.infrastructure.h2addapter.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "orderEntity")
public class OrderEntity {
    @Id
    private String id;
    @Column(name = "productId")
    private String productId;
    @Column(name = "quantity")
    private int quantity;


}
