package com.codeonce.kata.infrastructure.h2addapter.h2dbadapterorder;

import com.codeonce.kata.domain.model.Order;
import com.codeonce.kata.domain.port.spi.OrderPersistencePort;
import com.codeonce.kata.infrastructure.h2addapter.entities.OrderEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderH2DBAdapter implements OrderPersistencePort {

    private final OrderRepositoryH2 orderRepositoryH2;
    private final OrderMapperEntityModel orderMapperEntityModel;

    public OrderH2DBAdapter(OrderRepositoryH2 orderRepositoryH2, OrderMapperEntityModel orderMapperEntityModel) {
        this.orderRepositoryH2 = orderRepositoryH2;
        this.orderMapperEntityModel = orderMapperEntityModel;
    }

    @Override
    public Order createOrder(Order order) {
        OrderEntity orderEntity = orderMapperEntityModel.orderModelToOrderEntity(order);
        return orderMapperEntityModel.orderEntityToOrderModel(orderRepositoryH2.save(orderEntity));
    }

    @Override
    public List<Order> getAllOrders() {
        return orderRepositoryH2.findAll().stream().map(orderMapperEntityModel::orderEntityToOrderModel).collect(Collectors.toList());
    }
}
