package com.codeonce.kata.infrastructure.h2addapter.h2dbadapterorder;

import com.codeonce.kata.domain.model.Order;
import com.codeonce.kata.infrastructure.h2addapter.entities.OrderEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OrderMapperEntityModel {

    OrderEntity orderModelToOrderEntity(Order order);

    Order orderEntityToOrderModel(OrderEntity orderEntity);
}
