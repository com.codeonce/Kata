package com.codeonce.kata.infrastructure.h2addapter.h2dbadapterorder;

import com.codeonce.kata.infrastructure.h2addapter.entities.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepositoryH2 extends JpaRepository<OrderEntity, Long> {
}
