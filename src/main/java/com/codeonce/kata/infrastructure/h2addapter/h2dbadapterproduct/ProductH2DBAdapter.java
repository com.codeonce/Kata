package com.codeonce.kata.infrastructure.h2addapter.h2dbadapterproduct;

import com.codeonce.kata.domain.model.Product;
import com.codeonce.kata.domain.port.spi.ProductPersistencePort;
import com.codeonce.kata.infrastructure.h2addapter.entities.ProductEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductH2DBAdapter implements ProductPersistencePort {
    private final ProductRepositoryH2 productRepositoryH2;
    private final ProductMapperEntityModel productMapperEntityModel;

    public ProductH2DBAdapter(ProductRepositoryH2 productRepositoryH2, ProductMapperEntityModel productMapperEntityModel) {
        this.productRepositoryH2 = productRepositoryH2;
        this.productMapperEntityModel = productMapperEntityModel;
    }

    @Override
    public Product createProduct(Product product) {
        ProductEntity productEntity = productMapperEntityModel.productModelToProductEntity(product);
        return productMapperEntityModel.productEntityToProductModel(productRepositoryH2.save(productEntity));
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepositoryH2.findAll().stream().map(productMapperEntityModel::productEntityToProductModel).collect(Collectors.toList());
    }
}
