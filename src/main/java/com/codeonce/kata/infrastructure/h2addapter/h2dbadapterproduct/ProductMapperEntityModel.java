package com.codeonce.kata.infrastructure.h2addapter.h2dbadapterproduct;

import com.codeonce.kata.domain.model.Product;
import com.codeonce.kata.infrastructure.h2addapter.entities.ProductEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")

public interface ProductMapperEntityModel {

    ProductEntity productModelToProductEntity(Product product);

    Product productEntityToProductModel(ProductEntity productEntity);
}
