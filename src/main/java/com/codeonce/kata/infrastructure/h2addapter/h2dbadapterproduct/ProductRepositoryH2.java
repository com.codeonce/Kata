package com.codeonce.kata.infrastructure.h2addapter.h2dbadapterproduct;

import com.codeonce.kata.infrastructure.h2addapter.entities.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepositoryH2 extends JpaRepository<ProductEntity, Long> {
}
