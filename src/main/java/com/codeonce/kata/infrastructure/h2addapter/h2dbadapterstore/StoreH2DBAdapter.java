package com.codeonce.kata.infrastructure.h2addapter.h2dbadapterstore;

import com.codeonce.kata.domain.model.Store;
import com.codeonce.kata.domain.port.spi.StorePersistencePort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StoreH2DBAdapter implements StorePersistencePort {

    private final StoreRepositoryH2 storeRepositoryH2;
    private final StoreMapperEntityModel storeMapperEntityModel;

    public StoreH2DBAdapter(StoreRepositoryH2 storeRepositoryH2, StoreMapperEntityModel storeMapperEntityModel) {
        this.storeRepositoryH2 = storeRepositoryH2;
        this.storeMapperEntityModel = storeMapperEntityModel;

    }

    @Override
    public List<Store> getAllStores() {

        return storeRepositoryH2.findAll().stream().map(storeMapperEntityModel::storeEntityToStoreModel).collect(Collectors.toList());
    }
}
