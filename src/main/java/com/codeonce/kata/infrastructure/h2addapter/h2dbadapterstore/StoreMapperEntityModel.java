package com.codeonce.kata.infrastructure.h2addapter.h2dbadapterstore;

import com.codeonce.kata.domain.model.Store;
import com.codeonce.kata.infrastructure.h2addapter.entities.StoreEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")

public interface StoreMapperEntityModel {

    StoreEntity storeModelToStoreEntity(Store store);

    Store storeEntityToStoreModel(StoreEntity storeEntity);
}
