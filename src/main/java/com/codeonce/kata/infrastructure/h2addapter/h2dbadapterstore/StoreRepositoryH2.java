package com.codeonce.kata.infrastructure.h2addapter.h2dbadapterstore;

import com.codeonce.kata.infrastructure.h2addapter.entities.StoreEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreRepositoryH2 extends JpaRepository<StoreEntity, Long> {
}
