package com.codeonce.kata.infrastructure.kafkaconsumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
@Slf4j
@Component
public class KafkaConsumer {
    @KafkaListener(topics = "inventory", groupId = "inventory-service")
    public void consumeMessage(String message) {
        log.info("Received order: " + message);
    }
}
