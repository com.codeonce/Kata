package com.codeonce.kata.infrastructure.kafkaproducer;

import com.codeonce.kata.domain.model.Order;
import com.codeonce.kata.domain.port.spi.OrderPublishPort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component

public class KafkaAdapter implements OrderPublishPort {
    private static final String topic = "inventory";
    @Autowired
    KafkaTemplate<String, String> kafkaTemplate;

    @Override
    public void publish(Order order) {
        kafkaTemplate.send(topic, order.toString());

    }
}
