package com.codeonce.kata.controller;

import com.codeonce.kata.application.dto.OrderDto;
import com.codeonce.kata.application.mapper.OrderMapper;
import com.codeonce.kata.application.rest.ordercontroller.OrderController;
import com.codeonce.kata.domain.model.Order;
import com.codeonce.kata.domain.port.api.OrderServicePort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class TestOrderController {
    @InjectMocks
    private OrderController orderController;
    @Mock
    private OrderServicePort orderServicePort;
    @Mock
    private OrderMapper orderMapper;
    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(orderController).build();
    }

    @Test
    public void testCreateOrder() throws Exception {
        OrderDto orderDto = new OrderDto("1", "101", 5);
        Order order = new Order("1", "101", 5);
        when(orderMapper.toDomain(any(OrderDto.class))).thenReturn(order);
        when(orderServicePort.createOrder(any(Order.class))).thenReturn(order);
        when(orderMapper.toDto(any(Order.class))).thenReturn(orderDto);
        mockMvc.perform(post("/orders")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":\"1\",\"productId\":\"101\",\"quantity\":5}"))
                .andExpect(status().isCreated());
    }

    @Test
    public void testGetAllOrder() throws Exception {
        Order order = new Order("1", "56", 5);
        OrderDto orderDto = new OrderDto("1", "56", 5);
        when(orderServicePort.getAllOrders()).thenReturn(List.of(order));
        when(orderMapper.toDto(order)).thenReturn(orderDto);
        mockMvc.perform(get("/orders")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }
}
