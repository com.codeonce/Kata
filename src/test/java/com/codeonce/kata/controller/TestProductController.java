package com.codeonce.kata.controller;

import com.codeonce.kata.application.dto.ProductDto;
import com.codeonce.kata.application.mapper.ProductMapper;
import com.codeonce.kata.application.rest.productcontroller.ProductController;
import com.codeonce.kata.domain.model.Product;
import com.codeonce.kata.domain.port.api.ProductServicePort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest

public class TestProductController {
    @InjectMocks
    private ProductController productController;
    @Mock
    private ProductServicePort productServicePort;

    @Mock
    private ProductMapper productMapper;

    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(productController).build();
    }

    @Test
    public void testCreateProduct() throws Exception {
        ProductDto productDto = new ProductDto(1L, "Thon", "Thon EL MANAR", 4.0, "5");
        Product product = new Product(1L, "Thon", "Thon EL MANAR", 4.0, "5");
        when(productMapper.toDomain(any(ProductDto.class))).thenReturn(product);
        when(productServicePort.createProduct(any(Product.class))).thenReturn(product);
        when(productMapper.toDto(any(Product.class))).thenReturn(productDto);
        mockMvc.perform(post("/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                {
                                    "id": 1,
                                    "name": "Thon",
                                    "description": "Thon EL MANAR",
                                    "price": 4.0,
                                    "stockId": "5"
                                }
                                """))
                .andExpect(status().isCreated());
    }

    @Test
    public void testGetAllProduct() throws Exception {
        ProductDto productDto = new ProductDto(1L, "Thon", "Thon EL MANAR", 4.0, "5");
        Product product = new Product(1L, "Thon", "Thon EL MANAR", 4.0, "5");
        when(productServicePort.getAllProducts()).thenReturn(List.of(product));
        when(productMapper.toDto(product)).thenReturn(productDto);
        mockMvc.perform(get("/products")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }
}
