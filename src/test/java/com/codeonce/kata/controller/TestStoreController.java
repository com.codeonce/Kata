package com.codeonce.kata.controller;

import com.codeonce.kata.application.dto.StoreDto;
import com.codeonce.kata.application.mapper.StoreMapper;
import com.codeonce.kata.application.rest.storecontroller.StoreController;
import com.codeonce.kata.domain.model.Store;
import com.codeonce.kata.domain.port.api.StoreServicePort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class TestStoreController {
    @InjectMocks
    private StoreController storeController;
    @Mock
    private StoreServicePort storeServicePort;
    @Mock
    private StoreMapper storeMapper;

    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(storeController).build();
    }


    @Test
    public void testGetAllStore() throws Exception {
        Store store = new Store(1L, "Store A", "123 Main St");
        StoreDto storeDto = new StoreDto(1L, "Store A", "123 Main St");
        when(storeServicePort.getAllStores()).thenReturn(List.of(store));
        when(storeMapper.toDto(store)).thenReturn(storeDto);
        mockMvc.perform(get("/stores")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }
}
