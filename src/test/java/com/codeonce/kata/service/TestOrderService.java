package com.codeonce.kata.service;

import com.codeonce.kata.domain.model.Order;
import com.codeonce.kata.domain.port.spi.OrderPersistencePort;
import com.codeonce.kata.domain.port.spi.OrderPublishPort;
import com.codeonce.kata.domain.service.OrderServiceImpl;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
class TestOrderService {

    private OrderServiceImpl orderService;
    private OrderPersistencePort orderPersistencePort;
    private OrderPublishPort orderPublishPort;
    private SimpleMeterRegistry meterRegistry;
    private Counter orderCounter;

    @BeforeEach
    void setUp() {
        orderPersistencePort = mock(OrderPersistencePort.class);
        orderPublishPort = mock(OrderPublishPort.class);
        meterRegistry = new SimpleMeterRegistry();
        orderCounter = meterRegistry.counter("orders.created.total");
        orderService = new OrderServiceImpl(orderPersistencePort, orderPublishPort, meterRegistry);
    }

    @Test
    void testCreateOrderIncrementsCounter() {
        Order order = new Order("1", "56", 5);
        when(orderPersistencePort.createOrder(order)).thenReturn(order);
        Order result = orderService.createOrder(order);
        assertEquals(order, result);
        assertEquals(1.0, orderCounter.count());
        verify(orderPersistencePort).createOrder(order);
        verify(orderPublishPort).publish(order);
    }

    @Test
    void testGetAllOrders() {
        Order order1 = new Order("1", "56", 5);
        Order order2 = new Order("1", "57", 10);
        List<Order> orders = List.of(order1, order2);
        when(orderPersistencePort.getAllOrders()).thenReturn(orders);
        List<Order> result = orderService.getAllOrders();
        assertEquals(orders, result);
        verify(orderPersistencePort).getAllOrders();
    }
}
