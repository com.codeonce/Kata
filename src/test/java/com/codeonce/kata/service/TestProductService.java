package com.codeonce.kata.service;

import com.codeonce.kata.domain.model.Product;
import com.codeonce.kata.domain.port.spi.ProductPersistencePort;
import com.codeonce.kata.domain.service.ProductServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class TestProductService {

    private ProductServiceImpl productService;
    private ProductPersistencePort productPersistencePort;

    @BeforeEach
    void setUp() {
        productPersistencePort = mock(ProductPersistencePort.class);
        productService = new ProductServiceImpl(productPersistencePort);
    }

    @Test
    void testCreateProduct() {
        Product product = new Product(1L, "Thon", "Thon EL MANAR", 4.0, "5");
        when(productPersistencePort.createProduct(product)).thenReturn(product);
        Product result = productService.createProduct(product);
        assertEquals(product, result);
        verify(productPersistencePort).createProduct(product);
    }

    @Test
    void testGetAllProducts() {
        Product product1 = new Product(1L, "Thon", "Thon EL MANAR", 4.0, "5");
        Product product2 = new Product(2L, "Sardine", "Sardine EL MANAR", 3.0, "10");
        List<Product> products = List.of(product1, product2);
        when(productPersistencePort.getAllProducts()).thenReturn(products);
        List<Product> result = productService.getAllProducts();
        assertEquals(products, result);
        verify(productPersistencePort).getAllProducts();
    }
}
