package com.codeonce.kata.service;

import com.codeonce.kata.domain.model.Store;
import com.codeonce.kata.domain.port.spi.StorePersistencePort;
import com.codeonce.kata.domain.service.StoreServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class TestStoreService {

    private StoreServiceImpl storeService;
    private StorePersistencePort storePersistencePort;

    @BeforeEach
    void setUp() {
        storePersistencePort = mock(StorePersistencePort.class);
        storeService = new StoreServiceImpl(storePersistencePort);
    }

    @Test
    void testGetAllStores() {
        Store store1 = new Store(1L, "Store 1", "Address 1");
        Store store2 = new Store(2L, "Store 2", "Address 2");
        List<Store> stores = List.of(store1, store2);
        when(storePersistencePort.getAllStores()).thenReturn(stores);
        List<Store> result = storeService.getAllStores();
        assertEquals(stores, result);
        verify(storePersistencePort).getAllStores();
    }
}
